const articles = document.querySelectorAll("#article #play img");

articles.forEach((article) => {
  article.addEventListener("click", (e) => {
    const youtube =
      e.target.parentElement.parentElement.parentElement.dataset.youtube;
    const videoSection = e.target.parentElement.parentElement;
    // console.log(e.target.parentElement.parentElement);
    videoSection.innerHTML = `<iframe class="iframe-video-ngglobal-care-in-nevis" src="${youtube}"></iframe>`;
  });
});
