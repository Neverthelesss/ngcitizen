let activeAumot = 0;

const aumots = document.querySelectorAll("#aumot");

aumots[activeAumot].classList.remove("d-none");

function nextAumot() {
  if (activeAumot < aumots.length - 1) {
    aumots[activeAumot].classList.add("d-none");
    activeAumot = activeAumot + 1;
    aumots[activeAumot].classList.remove("d-none");
  } else {
    // console.log('mentok kanan');
    // console.log(activeAumot);
    activeAumot = 0;
    aumots[aumots.length-1].classList.add("d-none");
    aumots[0].classList.remove("d-none");
  }
}

function prevAumot() {
  if (activeAumot > 0) {
    aumots[activeAumot].classList.add("d-none");
    activeAumot = activeAumot - 1;
    aumots[activeAumot].classList.remove("d-none");
  } else {
    // console.log('mentok kiri');
    activeAumot = aumots.length - 1;
    aumots[activeAumot].classList.remove("d-none");
    aumots[0].classList.add("d-none");
  }
}
