let activeQpfs = 0;

const qpfs = document.querySelectorAll("#qpfs");

qpfs[activeQpfs].classList.replace("d-none", "d-flex");

function nextQpfs() {
  if (activeQpfs < qpfs.length - 1) {
    qpfs[activeQpfs].classList.replace("d-flex", "d-none");
    activeQpfs = activeQpfs + 1;
    qpfs[activeQpfs].classList.replace("d-none", "d-flex");
  } else {
    activeQpfs = 0;
    qpfs[activeQpfs].classList.replace("d-flex", "d-none");
    qpfs[0].classList.replace("d-none", "d-flex");
  }
}

function prevQpfs() {
  if (activeQpfs > 0) {
    qpfs[activeQpfs].classList.replace("d-flex", "d-none");
    activeQpfs = activeQpfs - 1;
    qpfs[activeQpfs].classList.replace("d-none", "d-flex");
  } else {
    activeQpfs = qpfs.length - 1;
    qpfs[activeQpfs].classList.replace("d-none", "d-flex");
    qpfs[0].classList.replace("d-flex", "d-none");
  }
}
// =========================================================
let activeQpfsX = 0;

const qpfsX = document.querySelectorAll("#qpfs-x");

qpfsX[activeQpfsX].classList.replace("d-none", "d-flex");

function nextQpfsX() {
  if (activeQpfsX < qpfsX.length - 1) {
    qpfsX[activeQpfsX].classList.replace("d-flex", "d-none");
    activeQpfsX = activeQpfsX + 1;
    qpfsX[activeQpfsX].classList.replace("d-none", "d-flex");
  } else {
    activeQpfsX = 0;
    qpfsX[activeQpfsX].classList.replace("d-flex", "d-none");
    qpfsX[0].classList.replace("d-none", "d-flex");
  }
}

function prevQpfsX() {
  if (activeQpfsX > 0) {
    qpfsX[activeQpfsX].classList.replace("d-flex", "d-none");
    activeQpfsX = activeQpfsX - 1;
    qpfsX[activeQpfsX].classList.replace("d-none", "d-flex");
  } else {
    activeQpfsX = qpfsX.length - 1;
    qpfsX[activeQpfsX].classList.replace("d-none", "d-flex");
    qpfsX[0].classList.replace("d-flex", "d-none");
  }
}