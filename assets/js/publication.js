let activePb = 0;

const pbs = document.querySelectorAll("#pb");

pbs[activePb].classList.remove("d-none");

function nextPb() {
  if (activePb < pbs.length - 1) {
    pbs[activePb].classList.add("d-none");
    activePb = activePb + 1;
    pbs[activePb].classList.remove("d-none");
  } else {
    activePb = 0;
    pbs[activePb].classList.add("d-none");
    pbs[0].classList.remove("d-none");
  }
}

function prevPb() {
  if (activePb > 0) {
    pbs[activePb].classList.add("d-none");
    activePb = activePb - 1;
    pbs[activePb].classList.remove("d-none");
  } else {
    activePb = pbs.length - 1;
    pbs[activePb].classList.remove("d-none");
    pbs[0].classList.add("d-none");
  }
}
