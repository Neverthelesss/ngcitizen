const gcOption = document.getElementById("global-citizenship-option");
const gcSticky = gcOption.offsetTop;

function optionFixTrigger() {
  if (window.innerWidth >= 1024) {
    if (window.pageYOffset >= gcSticky - 105) {
      gcOption.classList.add("global-citizenship-option-fix");
    } else {
      gcOption.classList.remove("global-citizenship-option-fix");
    }
  } else if (window.innerWidth >= 768) {
    if (window.pageYOffset >= gcSticky - 55) {
      gcOption.classList.add("global-citizenship-option-fix");
    } else {
      gcOption.classList.remove("global-citizenship-option-fix");
    }
  } else {
    if (window.pageYOffset >= gcSticky - 55) {
      gcOption.classList.add("global-citizenship-option-fix");
    } else {
      gcOption.classList.remove("global-citizenship-option-fix");
    }
  }
}


const spyScrolling = ( ) => {
    const sections = document.querySelectorAll( '.x-target-scrollSpy' );
  
    window.onscroll = ( ) => {
      const scrollPos = document.documentElement.scrollTop || document.body.scrollTop;
  
      optionFixTrigger()
      for ( let s in sections )
        if ( sections.hasOwnProperty( s ) && sections[ s ].offsetTop <= scrollPos + 400 ) {
          const id = sections[ s ].id;
          document.querySelector( '.active-scrollSpy' ).classList.remove( 'active-scrollSpy' );
          document.querySelector( `a[href*=${ id }]` ).parentNode.classList.add( 'active-scrollSpy' );
        }
    } 
  }

  const scrolToSection = (targetId) => {
    const id = targetId;
    const yOffset = -210; 
    const element = document.getElementById(id);
    const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;

    window.scrollTo({top: y, behavior: 'smooth'});

  }

spyScrolling();
