// eu / asia
const stdLux = document.getElementById("eu-or-asian");
const std = stdLux.children[0];
const lux = stdLux.children[1];

// detail
const stdLuxDetail = document.getElementById("std-or-lux-detail");
const stdDetail = stdLuxDetail.children[0];
const luxDetail = stdLuxDetail.children[1];

std.addEventListener("click", (e) => {
  std.classList.add("active");
  lux.classList.remove("active");
  stdDetail.style = null;
  luxDetail.style = "display: none !important";
});

lux.addEventListener("click", (e) => {
  std.classList.remove("active");
  lux.classList.add("active");
  stdDetail.style = "display: none !important";
  luxDetail.style = null;
});

let activeReis = 0;

const reis = document.querySelectorAll("#reis");

reis[activeReis].classList.replace("d-none", "d-flex");

function nextReis() {
  if (activeReis < reis.length - 1) {
    reis[activeReis].classList.replace("d-flex", "d-none");
    activeReis = activeReis + 1;
    reis[activeReis].classList.replace("d-none", "d-flex");
  } else {
    activeReis = 0;
    reis[activeReis].classList.replace("d-flex", "d-none");
    reis[0].classList.replace("d-none", "d-flex");
  }
}

function prevReis() {
  if (activeReis > 0) {
    reis[activeReis].classList.replace("d-flex", "d-none");
    activeReis = activeReis - 1;
    reis[activeReis].classList.replace("d-none", "d-flex");
  } else {
    activeReis = reis.length - 1;
    reis[activeReis].classList.replace("d-none", "d-flex");
    reis[0].classList.replace("d-flex", "d-none");
  }
}

let activeReil = 0;

const reil = document.querySelectorAll("#reil");

reil[activeReil].classList.replace("d-none", "d-flex");

function nextReil() {
  if (activeReil < reil.length - 1) {
    reil[activeReil].classList.replace("d-flex", "d-none");
    activeReil = activeReil + 1;
    reil[activeReil].classList.replace("d-none", "d-flex");
  } else {
    activeReil = 0;
    reil[activeReil].classList.replace("d-flex", "d-none");
    reil[0].classList.replace("d-none", "d-flex");
  }
}

function prevReil() {
  if (activeReil > 0) {
    reil[activeReil].classList.replace("d-flex", "d-none");
    activeReil = activeReil - 1;
    reil[activeReil].classList.replace("d-none", "d-flex");
  } else {
    activeReil = reil.length - 1;
    reil[activeReil].classList.replace("d-none", "d-flex");
    reil[0].classList.replace("d-flex", "d-none");
  }
}
