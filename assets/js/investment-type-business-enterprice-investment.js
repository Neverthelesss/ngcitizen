let activePbei = 0;

const pbeis = document.querySelectorAll("#pbeis");

pbeis[activePbei].classList.replace("d-none", "d-flex");

function nextPbeis() {
  if (activePbei < pbeis.length - 1) {
    pbeis[activePbei].classList.replace("d-flex", "d-none");
    activePbei = activePbei + 1;
    pbeis[activePbei].classList.replace("d-none", "d-flex");
  } else {
    activePbei = 0;
    pbeis[activePbei].classList.replace("d-flex", "d-none");
    pbeis[0].classList.replace("d-none", "d-flex");
  }
}

function prevPbeis() {
  if (activePbei > 0) {
    pbeis[activePbei].classList.replace("d-flex", "d-none");
    activePbei = activePbei - 1;
    pbeis[activePbei].classList.replace("d-none", "d-flex");
  } else {
    activePbei = pbeis.length - 1;
    pbeis[activePbei].classList.replace("d-none", "d-flex");
    pbeis[0].classList.replace("d-flex", "d-none");
  }
}
