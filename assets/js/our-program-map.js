const regions = document.querySelectorAll("#region");
const mapRegions = document.querySelectorAll("#map-region");

regions.forEach((data) => {
  data.addEventListener("click", (e) => {
    regions.forEach((data) => {
      data.style = "cursor: pointer;";
    });
    mapRegions.forEach((data) => {
      if (!data.classList.contains("d-none")) {
        data.classList.add("d-none");
      }
      if (data.dataset.mapRegion == e.target.dataset.region) {
        data.classList.remove("d-none");
      }
    });
    e.target.style = "border-bottom: solid #BE6A46;";
    // console.log(e.target.dataset.region);
  });
});
